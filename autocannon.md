#Install

npm i autocannon -g


# Test Server 
autocannon http://localhost:8081/fast/single -d 10 -c 100 -w 3 --renderStatusCodes --headers "Accept"="application/json"


# Test Client
autocannon http://localhost:8080/blocking/ -d 10 -c 100 -w 3 --renderStatusCodes --headers "Accept"="application/json"