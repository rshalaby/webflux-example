package net.rsworld.example.webflux.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.rsworld.example.webflux.shared.error.DefaultClientErrorHandler;
import net.rsworld.example.webflux.shared.error.DefaultDownStreamErrorResponseConverter;
import net.rsworld.example.webflux.shared.error.DownStreamErrorResponseConverter;
import net.rsworld.example.webflux.shared.error.ErrorFilterFunctions;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Slf4j
public class WebClientConfig {

  @Bean
  @ConfigurationProperties(prefix = "clients.slowvalueclient")
  public WebClientConfigParameters slowValueClientConfig() {
    return new WebClientConfigParameters();
  }

  @Bean
  @ConfigurationProperties(prefix = "clients.fastvalueclient")
  public WebClientConfigParameters fastValueClientConfig() {
    return new WebClientConfigParameters();
  }

  @Bean
  public DownStreamErrorResponseConverter defaultDownStreamErrorResponseConverter(
      ObjectMapper mapper) {
    return new DefaultDownStreamErrorResponseConverter(mapper);
  }

  @Bean
  public WebClient fastValueWebClient(
      WebClient.Builder webClientBuilder,
      WebClientConfigParameters fastValueClientConfig,
      DownStreamErrorResponseConverter converter) {

    log.info("FastValue Client Config: {}", fastValueClientConfig);

    return webClientBuilder
        .filter(ErrorFilterFunctions.handleErrors(new DefaultClientErrorHandler(), converter))
        .baseUrl(fastValueClientConfig.baseurl)
        .build();
  }

  @Bean
  public WebClient slowValueWebClient(
      WebClient.Builder webClientBuilder,
      WebClientConfigParameters slowValueClientConfig,
      DownStreamErrorResponseConverter converter) {

    log.info("SlowValue Client Config: {}", slowValueClientConfig);

    return webClientBuilder
        .filter(ErrorFilterFunctions.handleErrors(new DefaultClientErrorHandler(), converter))
        .baseUrl(slowValueClientConfig.baseurl)
        .build();
  }
}
