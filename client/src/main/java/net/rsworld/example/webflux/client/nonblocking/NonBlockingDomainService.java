package net.rsworld.example.webflux.client.nonblocking;

import net.rsworld.example.webflux.client.shared.CombinedValue;
import net.rsworld.example.webflux.client.shared.SimpleValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class NonBlockingDomainService {

  private final NonBlockingFastValueRestClient fastClient;
  private final NonBlockingSlowValueRestClient slowClient;

  @Autowired
  public NonBlockingDomainService(
      NonBlockingFastValueRestClient fastClient, NonBlockingSlowValueRestClient slowClient) {
    this.fastClient = fastClient;
    this.slowClient = slowClient;
  }

  public Mono<CombinedValue> processValues() {
    return fastClient
        .retrieveFastValue()
        .zipWith(slowClient.retrieveSlowValue(), this::combineValue);
  }

  private CombinedValue combineValue(SimpleValue value1, SimpleValue value2) {
    return CombinedValue.builder().value1(value1).value2(value2).build();
  }
}
