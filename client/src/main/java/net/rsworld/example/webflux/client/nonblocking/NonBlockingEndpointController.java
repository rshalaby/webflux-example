package net.rsworld.example.webflux.client.nonblocking;

import lombok.extern.slf4j.Slf4j;
import net.rsworld.example.webflux.client.shared.CombinedValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/nonblocking")
@Slf4j
public class NonBlockingEndpointController {

  final NonBlockingDomainService service;

  public NonBlockingEndpointController(NonBlockingDomainService service) {
    this.service = service;
  }

  @GetMapping("/")
  Mono<CombinedValue> singleSimpleValue() {
    log.info("Nonblocking Request");
    return service.processValues();
  }
}
