package net.rsworld.example.webflux.client.nonblocking;

import io.micrometer.tracing.Tracer;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import net.rsworld.example.webflux.client.shared.SimpleValue;
import net.rsworld.example.webflux.client.shared.SimpleValueDTO;
import net.rsworld.example.webflux.shared.exeption.DomainException;
import net.rsworld.example.webflux.shared.exeption.TechnicalException;
import net.rsworld.example.webflux.shared.health.ClientHealth;
import net.rsworld.example.webflux.shared.health.ReactiveClientHealthIndicator;
import net.rsworld.example.webflux.shared.otel.CorrelationIdConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class NonBlockingFastValueRestClient implements ReactiveClientHealthIndicator {

  private final WebClient webClient;

  private Tracer tracer;

  public NonBlockingFastValueRestClient(WebClient fastValueWebClient, Tracer tracer) {
    this.webClient = fastValueWebClient;
    this.tracer = tracer;
  }

  @Override
  public Mono<ClientHealth> getHealth() {
    return this.getHealth(webClient, "/mt/health", "FastValue Service", false);
  }

  public Mono<SimpleValue> retrieveFastValue() {
    return webClient
        .get()
        .uri("/fast/single")
        .accept(MediaType.APPLICATION_JSON)
        .header(
            CorrelationIdConstants.CORRELATION_ID_HEADER,
            tracer.getBaggage(CorrelationIdConstants.CORRELATION_ID_HEADER).get())
        .retrieve()
        .bodyToMono(SimpleValueDTO.class)
        .onErrorMap(unknownErrorPredicate(), this::createUnexpectedException)
        .map(SimpleValue::new)
        .doFinally(signal -> log.info("Fast Value retrieved." + readBaggages()));
  }

  private String readBaggages() {
    var baggage = tracer.getAllBaggage();
    return String.format("Bagggage = %1$s ", baggage);
  }

  private Predicate<? super Throwable> unknownErrorPredicate() {
    return Predicate.not(DomainException.class::isInstance);
  }

  private TechnicalException createUnexpectedException(Throwable cause) {
    return new TechnicalException("TF_999", "Unkown Error", cause);
  }
}
