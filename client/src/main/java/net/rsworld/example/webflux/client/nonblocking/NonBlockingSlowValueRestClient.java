package net.rsworld.example.webflux.client.nonblocking;

import lombok.extern.slf4j.Slf4j;
import net.rsworld.example.webflux.client.shared.SimpleValue;
import net.rsworld.example.webflux.client.shared.SimpleValueDTO;
import net.rsworld.example.webflux.shared.health.ClientHealth;
import net.rsworld.example.webflux.shared.health.ReactiveClientHealthIndicator;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class NonBlockingSlowValueRestClient implements ReactiveClientHealthIndicator {

  private final WebClient webClient;

  public NonBlockingSlowValueRestClient(WebClient slowValueWebClient) {
    this.webClient = slowValueWebClient;
  }

  @Override
  public Mono<ClientHealth> getHealth() {
    return this.getHealth(webClient, "/mt/health", "SlowValue Service", false);
  }

  public Mono<SimpleValue> retrieveSlowValue() {
    return webClient
        .get()
        .uri("/slow/single")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(SimpleValueDTO.class)
        .map(SimpleValue::new)
        .doFinally(signal -> log.info("Slow Value retrieved"));
  }
}
