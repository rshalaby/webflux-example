package net.rsworld.example.webflux.client.shared;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CombinedValue {

  SimpleValue value1;
  SimpleValue value2;
}
