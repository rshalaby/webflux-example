package net.rsworld.example.webflux.client.shared;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import lombok.Value;

@Value
public class SimpleValue {

  private String id;
  private String message;

  public SimpleValue(SimpleValueDTO input) {
    id = input.getId();
    message = decode(input.getMessage());
  }

  private String decode(String data) {
    return new String(Base64.getDecoder().decode(data), StandardCharsets.UTF_8);
  }
}
