package net.rsworld.example.webflux.client.nonblocking

import net.rsworld.example.webflux.client.WebfluxClientApplication
import net.rsworld.example.webflux.templates.TestTemplateConfig
import net.rsworld.example.webflux.templates.TestTemplateGenerator
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

import java.nio.charset.StandardCharsets

@SpringBootTest(classes = [WebfluxClientApplication.class, TestTemplateConfig.class])
@ActiveProfiles("test")
class NonBlockingClientSpec extends Specification {

    MockWebServer mockServer

    @Autowired
    NonBlockingFastValueRestClient client

    @Autowired
    @Qualifier("testTemplateGenerator")
    TestTemplateGenerator generator

    def setup() {
        mockServer = new MockWebServer()
        mockServer.start(9081)
    }

    def cleanup() {
        mockServer.shutdown()
    }

    def "can load client"() {
        expect:
        client != null
    }

    def "we are getting values"() {
        given:
        MockResponse mockResponse = new MockResponse()
        mockResponse.addHeader("Content-Type", MediaType.APPLICATION_JSON)
        def jsonBody = generator.process("/client/nonblocking/get-answer.json", Map.of("id", createTestID(), "message", createTestMessage()))
        mockResponse.setBody(jsonBody)
        mockServer.enqueue(mockResponse)

        when:
        def fastvalue = client.retrieveFastValue().block()

        then:
        def recordedRequest = mockServer.takeRequest()
        recordedRequest.getMethod() == "GET"
        recordedRequest.getPath() == "/fast/single"

        UUID.fromString(fastvalue.getId())
        fastvalue.getMessage().startsWith("Lorem ipsum dolor sit amet")

    }

    String encode(String data) {
        return Base64.getEncoder().encodeToString(data.getBytes(
                StandardCharsets.UTF_8))
    }

    String createTestID() {
        return UUID.randomUUID().toString()

    }

    String createTestMessage() {
        return encode(
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor "
                        + "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos"
                        + " et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea "
                        + "takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, "
                        + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore"
                        + " et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et "
                        + "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus"
                        + " est Lorem ipsum dolor sit amet."
        )
    }
}
