package net.rsworld.example.webflux.bdd.client.common;

import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.en.Then;
import net.rsworld.example.webflux.bdd.common.AbstractStepDefinitions;

public class CommonStepDefinitions extends AbstractStepDefinitions {

  @Then("HTTPStatus {int} is returned")
  public void http_status_is_returned(Integer httpStatus) {
    var response = testContext().getResponse();
    assertThat(response.getStatusCode()).isEqualTo(httpStatus);
  }
}
