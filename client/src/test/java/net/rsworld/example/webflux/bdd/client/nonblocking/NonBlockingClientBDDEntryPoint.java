package net.rsworld.example.webflux.bdd.client.nonblocking;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features/client/nonblocking")
@ConfigurationParameter(
    key = GLUE_PROPERTY_NAME,
    value =
        "net.rsworld.example.webflux.bdd.client.nonblocking, net.rsworld.example.webflux.bdd.client.common")
public class NonBlockingClientBDDEntryPoint {}
