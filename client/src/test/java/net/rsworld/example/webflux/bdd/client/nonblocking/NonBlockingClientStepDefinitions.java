package net.rsworld.example.webflux.bdd.client.nonblocking;

import static io.restassured.RestAssured.given;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;
import net.rsworld.example.webflux.bdd.common.AbstractStepDefinitions;
import net.rsworld.example.webflux.client.WebfluxClientApplication;
import net.rsworld.example.webflux.templates.TestTemplateConfig;
import net.rsworld.example.webflux.templates.TestTemplateGenerator;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(
    classes = {WebfluxClientApplication.class, TestTemplateConfig.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@CucumberContextConfiguration
@ActiveProfiles("test")
public class NonBlockingClientStepDefinitions extends AbstractStepDefinitions {

  private MockWebServer mockServer;

  @Autowired
  @Qualifier("testTemplateGenerator")
  TestTemplateGenerator generator;

  @Before
  public void before() throws IOException {
    mockServer = new MockWebServer();
    mockServer.start(9081);
  }

  @After
  public void after() throws IOException {
    testContext().reset();
    mockServer.close();
  }

  @When("a GET Request is made to {string}")
  public void a_get_request_is_made_to(String url) {
    MockResponse mockResponse = new MockResponse();
    mockResponse.addHeader("Content-Type", MediaType.APPLICATION_JSON);
    var jsonBody =
        generator.process(
            "/client/nonblocking/get-answer.json",
            Map.of("id", createTestID(), "message", createTestMessage()));
    mockResponse.setBody(jsonBody);
    mockServer.enqueue(mockResponse);
    mockServer.enqueue(mockResponse);

    Response response =
        given()
            .log()
            .all()
            .when()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .auth()
            .preemptive()
            .oauth2("mybearer")
            .get(baseUrl() + url);

    testContext().setResponse(response);
  }

  private String encode(String data) {
    return Base64.getEncoder().encodeToString(data.getBytes(StandardCharsets.UTF_8));
  }

  private String createTestID() {
    return UUID.randomUUID().toString();
  }

  private String createTestMessage() {
    return encode(
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor "
            + "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos"
            + " et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea "
            + "takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, "
            + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore"
            + " et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et "
            + "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus"
            + " est Lorem ipsum dolor sit amet.");
  }
}
