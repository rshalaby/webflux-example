package net.rsworld.example.webflux.bdd.common;

import static net.rsworld.example.webflux.bdd.common.TestContext.CONTEXT;

import org.springframework.boot.test.web.server.LocalServerPort;

public abstract class AbstractStepDefinitions {

  @LocalServerPort int localServerPort;

  public String baseUrl() {
    return "http://localhost:" + localServerPort;
  }

  public TestContext testContext() {
    return CONTEXT;
  }
}
