package net.rsworld.example.webflux.templates;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
public class TestTemplateConfig {

  @Bean
  public ITemplateResolver templateResolver() {
    var resolver = new ClassLoaderTemplateResolver();
    resolver.setPrefix("/features");
    resolver.setCacheable(true);
    resolver.setCacheTTLMs(null); // use LRU
    return resolver;
  }

  @Bean
  public TemplateEngine templateEngine(ITemplateResolver templateResolver) {
    var templateEngine = new TemplateEngine();
    templateEngine.setTemplateResolver(templateResolver);
    return templateEngine;
  }

  @Bean
  public TestTemplateGenerator testTemplateGenerator(TemplateEngine templateEngine) {
    return new TestTemplateGenerator(templateEngine);
  }
}
