package net.rsworld.example.webflux.templates;

import java.util.Locale;
import java.util.Map;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

public class TestTemplateGenerator {

  private final TemplateEngine templateEngine;

  public TestTemplateGenerator(TemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }

  public String process(String template, Map<String, Object> model) {
    var context = new Context(Locale.GERMAN, model);
    return this.templateEngine.process(template, context);
  }

  public String process(String template) {
    return process(template, Map.of("", ""));
  }
}
