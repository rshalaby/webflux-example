Feature: Test the NonBlocking Client E2E in Cucumber

  Scenario: NonBlocking Client gets calls
    When a GET Request is made to "/nonblocking/"
    Then HTTPStatus 200 is returned