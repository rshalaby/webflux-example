package net.rsworld.example.webflux.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/fast")
@Slf4j
public class FastEndpointController {

  @GetMapping("/single")
  Mono<SimpleValueDTO> singleSimpleValue() {
    log.info("Fast Value Request");
    return Mono.just(SimpleValue.createTestValue().convert());
  }
}
