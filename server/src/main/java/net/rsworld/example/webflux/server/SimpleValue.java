package net.rsworld.example.webflux.server;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Value
@Builder
@Slf4j
public class SimpleValue {

  private String id;
  private String message;

  public SimpleValueDTO convert() {
    return SimpleValueDTO.builder().id(id).message(encode(message)).build();
  }

  private String encode(String data) {
    return Base64.getEncoder().encodeToString(data.getBytes(StandardCharsets.UTF_8));
  }

  public static SimpleValue createTestValue() {
    log.info("Creating random value");
    return SimpleValue.builder()
        .id(UUID.randomUUID().toString())
        .message(
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor "
                + "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos"
                + " et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea "
                + "takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, "
                + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore"
                + " et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et "
                + "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus"
                + " est Lorem ipsum dolor sit amet.")
        .build();
  }
}
