package net.rsworld.example.webflux.server;

import net.rsworld.example.webflux.server.repository.SimpleValueEntity;
import net.rsworld.example.webflux.server.repository.SimpleValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class SimpleValueService {

  private final SimpleValueRepository repository;

  @Autowired
  public SimpleValueService(SimpleValueRepository repository) {
    this.repository = repository;
  }

  private static SimpleValue convertSimpleEntity(SimpleValueEntity s) {
    return SimpleValue.builder().message(s.getMessage()).id(s.getId()).build();
  }

  public Mono<SimpleValue> getMessagebyId(String id) {
    return repository.findById(id).map(SimpleValueService::convertSimpleEntity);
  }
}
