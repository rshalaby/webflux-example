package net.rsworld.example.webflux.server;

import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/slow")
@Slf4j
public class SlowEndpointController {

  @GetMapping("/single")
  public Mono<SimpleValueDTO> singleSimpleValue() {
    log.info("Slow Value Request");
    return Mono.just(SimpleValue.createTestValue().convert())
        .delaySubscription(Duration.ofMillis(500));
  }
}
