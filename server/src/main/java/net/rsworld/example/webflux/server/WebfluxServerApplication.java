package net.rsworld.example.webflux.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Hooks;

@SpringBootApplication(scanBasePackages = "net.rsworld.example.webflux")
public class WebfluxServerApplication {

  public static void main(String[] args) {
    // Automatic MDC propagation needed for Tracing to work with Webflux
    // see: https://github.com/spring-projects/spring-boot/issues/34201
    Hooks.enableAutomaticContextPropagation();
    SpringApplication.run(WebfluxServerApplication.class, args);
  }
}
