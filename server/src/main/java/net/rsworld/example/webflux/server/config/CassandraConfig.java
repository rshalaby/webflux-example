package net.rsworld.example.webflux.server.config;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.CqlSessionBuilder;
import com.datastax.oss.driver.api.querybuilder.SchemaBuilder;
import java.net.InetSocketAddress;
import org.cognitor.cassandra.migration.spring.CassandraMigrationAutoConfiguration;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.data.cassandra.config.DefaultCqlBeanNames;

@Configuration
public class CassandraConfig {

  @Bean(CassandraMigrationAutoConfiguration.CQL_SESSION_BEAN_NAME)
  public CqlSession cassandraMigrationCqlSession(CassandraProperties props) {
    var migrationSession = createSession(props).build();
    var createKeySpace =
        SchemaBuilder.createKeyspace(props.getKeyspaceName()).ifNotExists().withSimpleStrategy(1);
    migrationSession.execute(createKeySpace.build());
    return migrationSession;
  }

  @Bean(DefaultCqlBeanNames.SESSION)
  @Primary
  // ensure that the keyspace is created if needed before initializing spring-data session
  @DependsOn(CassandraMigrationAutoConfiguration.MIGRATION_TASK_BEAN_NAME)
  public CqlSession cassandraSession(CassandraProperties props) {
    return createSession(props).withKeyspace(props.getKeyspaceName()).build();
  }

  private CqlSessionBuilder createSession(CassandraProperties props) {
    return CqlSession.builder()
        .addContactPoints(
            props.getContactPoints().stream()
                .map(contactPoint -> new InetSocketAddress(contactPoint, props.getPort()))
                .toList())
        .withLocalDatacenter(props.getLocalDatacenter())
        .withAuthCredentials(props.getUsername(), props.getPassword());
  }
}
