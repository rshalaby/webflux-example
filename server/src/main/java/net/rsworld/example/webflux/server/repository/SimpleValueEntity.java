package net.rsworld.example.webflux.server.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("simple_values")
@Data
@AllArgsConstructor
public class SimpleValueEntity {

  @PrimaryKey private String id;
  private String message;
}
