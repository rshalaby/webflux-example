package net.rsworld.example.webflux.server.repository;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

public interface SimpleValueRepository
    extends ReactiveCassandraRepository<SimpleValueEntity, String> {}
