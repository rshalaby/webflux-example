package net.rsworld.example.webflux.server.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@SpringBootTest
class SimpleValueRepositoryTest {

  @Autowired SimpleValueRepository repository;

  @BeforeEach
  void setUp() {
    Flux<SimpleValueEntity> deleteAndInsert =
        repository
            .deleteAll()
            .thenMany(repository.saveAll(Flux.just(new SimpleValueEntity("1234", "mymessage"))));

    StepVerifier.create(deleteAndInsert).expectNextCount(1).verifyComplete();
  }

  @Test
  void testSomething() {
    assertTrue(true);
  }

  @AfterEach
  void tearDown() {}
}
