package net.rsworld.example.webflux.shared.error;

import net.rsworld.example.webflux.shared.exeption.DomainException;
import reactor.core.publisher.Mono;

public interface ClientErrorHandler {

  Mono<DomainException> handle(Mono<DownStreamError> error);
}
