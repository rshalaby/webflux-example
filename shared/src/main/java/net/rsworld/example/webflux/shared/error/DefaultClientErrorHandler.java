package net.rsworld.example.webflux.shared.error;

import net.rsworld.example.webflux.shared.exeption.DomainException;
import net.rsworld.example.webflux.shared.exeption.TechnicalException;
import reactor.core.publisher.Mono;

public class DefaultClientErrorHandler implements ClientErrorHandler {

  @Override
  public Mono<DomainException> handle(Mono<DownStreamError> error) {
    return error.map(this::createException);
  }

  private DomainException createException(DownStreamError error) {
    return new TechnicalException(
        error.getErrorCode(),
        String.format(
            "HTTP Error (Status: %1$s): %2$s", error.getHttpStatus(), error.getMessage()));
  }
}
