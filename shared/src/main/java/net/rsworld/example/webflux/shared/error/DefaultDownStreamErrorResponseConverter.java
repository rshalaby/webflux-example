package net.rsworld.example.webflux.shared.error;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

public class DefaultDownStreamErrorResponseConverter implements DownStreamErrorResponseConverter {

  private final ObjectMapper mapper;

  public DefaultDownStreamErrorResponseConverter(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public Mono<DownStreamError> convert(ClientResponse response) {
    return response
        .bodyToMono(String.class)
        .map(this::convertBody)
        .map(e -> e.convert(response.statusCode().value()));
  }

  private DownStreamErrorDTO convertBody(String body) {
    if (body != null) {
      try {
        return mapper.readValue(body, DownStreamErrorDTO.class);
      } catch (JsonProcessingException e) {
        return new DownStreamErrorDTO("", body);
      }
    }
    return new DownStreamErrorDTO("", "no message");
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  class DownStreamErrorDTO {

    private String errorCode;
    private String message;

    DownStreamError convert(int httpStatus) {
      return DownStreamError.builder()
          .errorCode(errorCode)
          .httpStatus(httpStatus)
          .message(message)
          .build();
    }
  }
}
