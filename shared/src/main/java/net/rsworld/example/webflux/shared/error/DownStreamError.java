package net.rsworld.example.webflux.shared.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DownStreamError {

  private int httpStatus;
  private String errorCode;
  private String message;

}
