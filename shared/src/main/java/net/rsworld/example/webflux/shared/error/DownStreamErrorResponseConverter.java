package net.rsworld.example.webflux.shared.error;

import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

public interface DownStreamErrorResponseConverter {

  Mono<DownStreamError> convert(ClientResponse response);
}
