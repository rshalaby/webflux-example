package net.rsworld.example.webflux.shared.error;

import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import reactor.core.publisher.Mono;

public abstract class ErrorFilterFunctions {

  private ErrorFilterFunctions() {}

  public static ExchangeFilterFunction handleErrors(
      ClientErrorHandler errorHandler, DownStreamErrorResponseConverter errorResponseConverter) {
    return ExchangeFilterFunction.ofResponseProcessor(
        response ->
            response.statusCode().value() >= 400
                ? errorHandler.handle(errorResponseConverter.convert(response)).flatMap(Mono::error)
                : Mono.just(response));
  }
}
