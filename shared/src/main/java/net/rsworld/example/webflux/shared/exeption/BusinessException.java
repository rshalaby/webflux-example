package net.rsworld.example.webflux.shared.exeption;

public class BusinessException extends DomainException {

  public BusinessException(String errorCode, String message) {
    super(errorCode, message);
  }
}
