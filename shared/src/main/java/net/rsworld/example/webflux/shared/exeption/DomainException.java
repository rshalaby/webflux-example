package net.rsworld.example.webflux.shared.exeption;

public abstract class DomainException extends RuntimeException {

  private final String errorCode;

  protected DomainException(String errorCode, String message) {
    this(errorCode, message, null);
  }

  protected DomainException(String errorCode, String message, Throwable cause) {
    super(message, cause);
    this.errorCode = errorCode;
  }

  public String getErrorCode() {
    return this.errorCode;
  }
}
