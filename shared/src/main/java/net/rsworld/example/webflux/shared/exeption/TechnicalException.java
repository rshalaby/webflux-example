package net.rsworld.example.webflux.shared.exeption;

public class TechnicalException extends DomainException {

  public TechnicalException(String errorCode, String message) {
    this(errorCode, message, null);
  }

  public TechnicalException(String errorCode, String message, Throwable cause) {
    super(errorCode, message, cause);
  }
}
