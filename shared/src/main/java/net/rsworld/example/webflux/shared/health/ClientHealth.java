package net.rsworld.example.webflux.shared.health;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.NonNull;
import lombok.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.util.StringUtils;

@Value
@Builder
public class ClientHealth {

  @NonNull Status status;
  @Default boolean optional = true;
  @NonNull String clientName;
  String information;

  public String createStatusInformation() {
    var result = new StringBuilder(status.getCode());
    if (StringUtils.hasText(information)) {
      result.append(" - ");
      result.append(information);
    }
    return result.toString();
  }
}
