package net.rsworld.example.webflux.shared.health;

import java.util.List;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.boot.actuate.health.ReactiveHealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component("serviceClients")
public class CombinedClientsHealthContributor implements ReactiveHealthIndicator {

  private final List<ReactiveClientHealthIndicator> clients;

  public CombinedClientsHealthContributor(List<ReactiveClientHealthIndicator> clients) {
    this.clients = clients;
  }

  @Override
  public Mono<Health> health() {
    var result = Health.up(); // we guess we are up otherwise we would not be here :)
    return Flux.fromStream(clients.stream())
        .flatMap(ReactiveClientHealthIndicator::getHealth)
        .reduce(result, this::processHealth)
        .map(Builder::build);
  }

  private Health.Builder processHealth(Health.Builder healthBuilder, ClientHealth clientHealth) {
    var result =
        healthBuilder.withDetail(
            clientHealth.getClientName(), clientHealth.createStatusInformation());

    // if a required service is not up, we are down
    return (!clientHealth.isOptional() && !clientHealth.getStatus().equals(Status.UP)
        ? result.down()
        : result);
  }
}
