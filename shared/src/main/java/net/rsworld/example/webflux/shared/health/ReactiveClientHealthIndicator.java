package net.rsworld.example.webflux.shared.health;

import org.springframework.boot.actuate.health.Status;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public interface ReactiveClientHealthIndicator {

  Mono<ClientHealth> getHealth();

  /**
   * Default implementation for a Health Check. Mark a service as UP when the Health Check of the
   * service returns Statuscode 200. Every other Status Code will mark the service as DOWN
   *
   * @param webClient client that should be used to make the request
   * @param healthUri path to the health check of the service
   * @param serviceName name of the service, will be displayed at the actuator
   * @param optional is the service essentiell or optional ?
   * @return Client health object
   */
  default Mono<ClientHealth> getHealth(
      WebClient webClient, String healthUri, String serviceName, boolean optional) {

    return webClient
        .get()
        .uri(healthUri)
        .exchangeToMono(
            response -> {
              if (response.statusCode().equals(HttpStatus.OK)) {
                return Mono.just(
                    ClientHealth.builder()
                        .status(Status.UP)
                        .clientName(serviceName)
                        .optional(optional)
                        .build());
              } else {
                return response.createException().flatMap(Mono::error);
              }
            })
        .onErrorResume(
            err ->
                Mono.just(
                    ClientHealth.builder()
                        .status(Status.DOWN)
                        .clientName(serviceName)
                        .information(err.getMessage())
                        .optional(optional)
                        .build()));
  }
}
