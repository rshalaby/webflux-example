package net.rsworld.example.webflux.shared.otel;

public class CorrelationIdConstants {

  private CorrelationIdConstants() {}

  public static final String CORRELATION_ID_HEADER = "CorrelationId";
  public static final String TRACE_ID = "trace_id";
  public static final String SPAN_ID = "span_id";
  public static final String CORRELATION_ID = "CorrelationId";
}
