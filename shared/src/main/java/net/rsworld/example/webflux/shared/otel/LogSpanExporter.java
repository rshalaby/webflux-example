package net.rsworld.example.webflux.shared.otel;

import io.opentelemetry.sdk.common.CompletableResultCode;
import io.opentelemetry.sdk.common.InstrumentationScopeInfo;
import io.opentelemetry.sdk.trace.data.EventData;
import io.opentelemetry.sdk.trace.data.SpanData;
import io.opentelemetry.sdk.trace.export.SpanExporter;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * An example {@link SpanExporter} implementation that logs every span it exports.
 *
 * <p>Has one problem: Exporting happens in a separate thread after the span closes. So the
 * timestamp of the log message is the time of the export, not the time of the event. The timestamp
 * of the event is in the MDC attribute "timestamp".
 *
 * <p>The exporter use a special named logger "PFCR" to log the spans.
 *
 * <p>Use with SpanExporterLogAppender to log the correct timestamp.
 */
public final class LogSpanExporter implements SpanExporter {

  public static LogSpanExporter create() {
    return new LogSpanExporter();
  }

  private static Logger log = LoggerFactory.getLogger("PFCR");

  private LogSpanExporter() {}

  /**
   * Exports a batch of {@link SpanData} to the logging framework.
   *
   * @param spans the batch of {@link SpanData} to be exported.
   * @return the result of the export.
   */
  @Override
  public CompletableResultCode export(Collection<SpanData> spans) {

    for (SpanData span : spans) {
      try {
        if (log.isInfoEnabled()) {
          MDC.put("trace_id", span.getTraceId());
          MDC.put("span_id", span.getSpanId());
          MDC.put("span_name", span.getName());
          MDC.put("span_kind", span.getKind().name());
          MDC.put("timestamp", String.valueOf(span.getStartEpochNanos()));
          log.info(buildSpanLog(span));
          for (EventData event : span.getEvents()) {
            if (event.getName().equals("LogEvent")) {
              MDC.put("timestamp", String.valueOf(event.getEpochNanos()));
              log.info("{}", event.getAttributes());
            }
          }
        }
      } finally {
        MDC.clear();
      }
    }
    return CompletableResultCode.ofSuccess();
  }

  private String buildSpanLog(SpanData span) {
    StringBuilder sb = new StringBuilder(60);
    InstrumentationScopeInfo instrumentationScopeInfo = span.getInstrumentationScopeInfo();
    sb.append("'")
        .append(" [tracer: ")
        .append(instrumentationScopeInfo.getName())
        .append(":")
        .append(
            instrumentationScopeInfo.getVersion() == null
                ? ""
                : instrumentationScopeInfo.getVersion())
        .append("] ")
        .append(span.getAttributes());
    return sb.toString();
  }

  @Override
  public CompletableResultCode flush() {
    CompletableResultCode resultCode = new CompletableResultCode();
    return resultCode.succeed();
  }

  @Override
  public CompletableResultCode shutdown() {
    return flush();
  }
}
