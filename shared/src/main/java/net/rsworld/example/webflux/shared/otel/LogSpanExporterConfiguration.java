package net.rsworld.example.webflux.shared.otel;

import org.springframework.context.annotation.Configuration;

/** Configuration for the {@link LogSpanExporter}. */
public class LogSpanExporterConfiguration {

  private LogSpanExporterConfiguration() {}

  @Configuration
  static class OpenTelemetryConfiguration {

    // @Bean
    LogSpanExporter loggingSpanExporter() {
      return LogSpanExporter.create();
    }
  }
}
