package net.rsworld.example.webflux.shared.otel.logback;

import static net.rsworld.example.webflux.shared.otel.CorrelationIdConstants.CORRELATION_ID_HEADER;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import ch.qos.logback.core.spi.AppenderAttachable;
import ch.qos.logback.core.spi.AppenderAttachableImpl;
import io.opentelemetry.api.baggage.Baggage;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanContext;
import io.opentelemetry.sdk.trace.ReadableSpan;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.rsworld.example.webflux.shared.otel.CorrelationIdConstants;

public class OpenTelemetryLogAppender extends UnsynchronizedAppenderBase<ILoggingEvent>
    implements AppenderAttachable<ILoggingEvent> {

  private final AppenderAttachableImpl<ILoggingEvent> aai = new AppenderAttachableImpl<>();

  public static ILoggingEvent wrapEvent(ILoggingEvent event) {

    if (Span.current() instanceof ReadableSpan currentSpan) {

      if (!currentSpan.getSpanContext().isValid()) {
        return event;
      }

      Map<String, String> eventContext = new HashMap<>(event.getMDCPropertyMap());
      if (eventContext.containsKey(CorrelationIdConstants.TRACE_ID)) {
        // Assume already instrumented event if traceId is present.
        return event;
      }

      SpanContext spanContext = currentSpan.getSpanContext();
      eventContext.put(CorrelationIdConstants.TRACE_ID, spanContext.getTraceId());
      eventContext.put(CorrelationIdConstants.SPAN_ID, spanContext.getSpanId());

      Baggage baggage = Baggage.current();
      eventContext.put(
          CorrelationIdConstants.CORRELATION_ID, baggage.getEntryValue(CORRELATION_ID_HEADER));

      return new LoggingEventWrapper(event, eventContext);
    } else {
      Map<String, String> eventContext = event.getMDCPropertyMap();
      return new LoggingEventWrapper(event, eventContext);
    }
  }

  @Override
  protected void append(ILoggingEvent event) {
    aai.appendLoopOnAppenders(wrapEvent(event));
  }

  @Override
  public void addAppender(Appender<ILoggingEvent> appender) {
    aai.addAppender(appender);
  }

  @Override
  public Iterator<Appender<ILoggingEvent>> iteratorForAppenders() {
    return aai.iteratorForAppenders();
  }

  @Override
  public Appender<ILoggingEvent> getAppender(String name) {
    return aai.getAppender(name);
  }

  @Override
  public boolean isAttached(Appender<ILoggingEvent> appender) {
    return aai.isAttached(appender);
  }

  @Override
  public void detachAndStopAllAppenders() {
    aai.detachAndStopAllAppenders();
  }

  @Override
  public boolean detachAppender(Appender<ILoggingEvent> appender) {
    return aai.detachAppender(appender);
  }

  @Override
  public boolean detachAppender(String name) {
    return aai.detachAppender(name);
  }
}
